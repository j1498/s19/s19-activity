
const number = 2

const getCube = number ** 3;
console.log(`The cube of ${number} is ${getCube}`);


// Desctructure Array
const Address = ["Mongpong", "Roxas City", "Capiz"];

const [brgy, city, province] = Address;

console.log(`I live at ${brgy}, ${city}, ${province}. `);


// Variable/Object

const animal = {
		name: "Lolong",
		type: "saltwater crocodile",
		weight: 1075,
		measurement: "20 ft 3 in",
}

const{name, type, weight, measurement} = animal

console.log(`${name} was a ${type}. He weighted at ${weight} kgs with a measurement of ${measurement}`);


// Array of numbers

const numbers = [1, 2, 3, 4, 5];
numbers.forEach( numb => console.log(numb));



class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Toffie", 1, "Japanese Spitz");
console.log(myDog)

